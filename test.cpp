#include <bits/stdc++.h>
using namespace std;

bool s(int a[], int n)
{
    if (n == 2 && a[n - 1] > a[n - 2])
    {
        return true;
    }
    else if (n > 2 && a[n - 1] > a[n - 2])
    {
        return s(a, n - 1);
    }
    return false;
}
void f(int A[], int lastIndex)
{
    if (lastIndex >= 1)
    {
        f(A, lastIndex - 1);
        int last = A[lastIndex];
        int i = lastIndex - 1;
        while (i >= 0 && A[i] > last)
        {
            A[i + 1] = A[i];
            i--;
        }
        A[i + 1] = last;
    }
}
int t(int n)
{
    if (n == 1)
        return 0;
    return 2 * t(n - 1) + 2;
}

int findLowerBound(int x, int A[], int low, int high)
{
    if (A[low] <= x)
    {
        if (low < high)
        {
            int mid = (low + high) / 2;
            if (A[mid] <= x)
            {
                return findLowerBound(x, A, low, mid - 1);
            }
        }
        return low;
    }
    return -1;
}
int a[6] = {0};
int fun(int n)
{   
    a[n]++;
    cout << "fun" << n << " " << a[0] << endl;
    int x = 1;
    if (n == 1)
        return x;
    for (int k = 1; k < n; k++)
        x += fun(k) * fun(n - k);
    return x;
    
}

int main()
{
    for (int i = 1; i <= 10; i++)
        cout << t(i) << " ";
    //int a[] = {2, 3, 4, 4, 9, 10};
    // f(a, 1);
    //cout << findLowerBound(2, a, 1, 5);
    
    // for (int i = 0; i < 6; i++)
    // {
    //     cout << a[i] << " ";
    // }
    //cout << s(a, 5);
    return 0;
}